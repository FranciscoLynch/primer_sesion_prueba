// configuracion de los frameworks 

const express = require('express'); 
const server = express(); 
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express'); 

server.use(express.json()); 
server.use(express.urlencoded({ extended: true}));

const swaggerOptions = { 
    swaggerDefinition: { 
        info: { 
            title: 'Workshop', 
            version: '1.0.0'
        }
    }, 
    apis: ['./server.js'],
}; 

const swaggerDocs = swaggerJsDoc(swaggerOptions); 

server.use('/api-docs', 
    swaggerUI.serve, 
    swaggerUI.setup(swaggerDocs)); 


// desarrollo de la api 

// ESTRUCTURA DE DATOS 

const users = [{ 
    id: 1,  
    usuario: 'administrador',
    nom_ape: 'carlos rodriguez', 
    email: 'admin@email.com', 
    contraseña: '123',  
    admin: true, 
    estado: false
}]; 
const user = {
    id: 2, 
    usuario: 'juancito',
    nom_ape: 'Juan Perez', 
    email: 'juan@email.com', 
    telefono: 222222,  
    direccion: ' ',
    contraseña: '12345',  
    admin: false, 
    estado: false
}; 

users.push(user); 
console.log(users);

const products = [ 
    { 
        id:Number, 
        stock:Number, 
        name: String
    }, 
]; 

const orders = [ 
    { 
        id: Number, 
        product: [ 
            { 
                id:Number, 
                name:String
            }
        ], 
        amount:Number, 
        id_user:Number,
    }
];


// middlewares - VALIDACIONES

const Admininistrador = (req,res,next) => { 
    let id_user = req.body.id; 
    let flag = false; 
    users.forEach(user => { 
        if (user.id == id_user){ 
            if(user.admin == true){ 
                flag = true;
            }
        }
    }); 
    if (flag == false){ 
        res.json('El usuario no es administrador'); 
    } else { 
        next();
    }
} 

const logueado = (req,res,next) => { 
    let id_user = req.body.id; 
    let flag = false; 
    users.forEach(user => { 
        if (user.id == id_user){
            if (user.estado == true){ 
                flag = true;
            }
        }
    }); 
    if (flag == false){ 
        res.json('El usuario no esta logueado'); 
    } else { 
        next();
    }
}  

const validarCampos = (req,res,next) => { 
    if (req.body.id !=="" || req.body.usuario !=="" || req.body.nom_ape !=="" || req.body.email !=="" || req.body.telefono !=="" || req.body.direccion !=="" || req.body.contraseña !=="" ) { 
        next();
    } else { 
        res.status(404).send('Se requiere completar todos los campos');
    }
};

// endpoints 

/**
 * @swagger
 * /login:
 *  post:
 *    descriprion: Verificar existencia del usuario
 *    parameters: 
 *    - name: usuario
 *      type: string
 *      in: formData
 *      required: true
 *      description : nombre del usuario
 *    - name: contraseña
 *      type: string
 *      in: formData
 *      required: true
 *      description : contraseña del usuario
 *    responses:
 *      200:
 *        Sucess
 */

server.post('/login', (req,res) => { 
    const {usuario, contraseña } = req.body; 
    console.log(usuario, contraseña); 
    let index = -1; 
    users.forEach((user, i) => {
        if (user.usuario == usuario){
            if (user.contraseña == contraseña){ 
                index = i;
            }
        }
    }); 
    if (index == -1){ 
        console.log('error'); 
        res.send(false);
    } else { 
        users[index].estado = true; 
        console.log(user[index]); 
        res.json(index);
    }
});  

/* 
server.post('/users', (req,res) => { 
    const {usuario, contraseña } = req.body; 
    console.log(usuario, contraseña); 
    let index = -1; 
    users.forEach((user, i) => {
        if (user.usuario == usuario){
            if (user.contraseña == contraseña){ 
                index = i;
            }
        }
    }); 
    if (index == -1){ 
        console.log('error'); 
        res.send(false);
    } else { 
        users[index].estado = true; 
        console.log(user[index]); 
        res.json(index);
    }
}); 
 */ 


/**
 * @swagger
 * /products:
 *  get:
 *    description: Muestra los productos
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.get('/products', logueado, (req, res) => { 
    res.send('Todos los productos');
}); 


/**
 * @swagger
 * /users/high:
 *  post:
 *    descriprion: Creacion de usuario
 *    parameters: 
 *    - name: id
 *      type: integer
 *      in: formData
 *      required: true
 *      description : id del usuario
 *    - name: usuario
 *      type: string
 *      in: formData
 *      required: true
 *      description : nombre de usuario
 *    - name: nom_ape
 *      type: string
 *      in: formData
 *      required: true
 *      description : nombre y apellido del usuarios
 *    - name: email
 *      type: string
 *      in: formData
 *      required: true
 *      description: correo electronico del usuario
 *    - name: telefono
 *      type: integer
 *      in: formData
 *      required: true
 *      description: telefono del usuario
 *    - name: direccion
 *      type: string
 *      in: formData
 *      required: true
 *      description: direccion del usuario
 *    - name: contraseña
 *      type: string
 *      in: formData
 *      required: true
 *      description: contraseña del usuario
 *    responses:
 *      200:
 *        Sucess
 */

server.post('/users/high', Admininistrador, validarCampos,(req,res) => { 
    const {id, usuario, nom_ape, email, telefono, direccion, contraseña} = req.body; 
    const usuario_ = { 
        id: id, 
        usuario: usuario,
        nom_ape: nom_ape, 
        email: email, 
        telefono: telefono, 
        direccion: direccion, 
        contraseña: contraseña,
        admin: false,
        estado: false
    }; 
    users.push(usuario_); 
    console.log('Usuario creado exitosamente');
    res.send(usuario_);
});  

/**
 * @swagger
 * /users/mod:
 *  post:
 *    descriprion: Modificacion de usuario
 *    parameters: 
 *    - name: id
 *      type: integer
 *      in: formData
 *      required: true
 *      description : id del usuario
 *    - name: usuario
 *      type: string
 *      in: formData
 *      required: true
 *      description : nombre de usuario
 *    - name: nom_ape
 *      type: string
 *      in: formData
 *      required: true
 *      description : nombre y apellido del usuarios
 *    - name: email
 *      type: string
 *      in: formData
 *      required: true
 *      description: correo electronico del usuario
 *    - name: telefono
 *      type: integer
 *      in: formData
 *      required: true
 *      description: telefono del usuario
 *    - name: direccion
 *      type: string
 *      in: formData
 *      required: true
 *      description: direccion del usuario
 *    - name: contraseña
 *      type: string
 *      in: formData
 *      required: true
 *      description: contraseña del usuario
 *    responses:
 *      200:
 *        Sucess
 */

server.put('/users/mod', Admininistrador, validarCampos,(req,res) => { 
    const {id,usuario,nom_ape,email,telefono,direccion,contraseña} = req.body; 
    const user_mod = { 
        id: id, 
        usuario: usuario,
        nom_ape: nom_ape, 
        email: email, 
        telefono: telefono, 
        direccion: direccion, 
        contraseña: contraseña
    }; 
    let user_id = user_mod.id;
    usuarios.forEach(usuario => { 
        if(usuario.id = user_id) { 
            usuario.id = user_mod.id;
            usuario.usuario = user_mod.usuario
            usuario.nom_ape = user_mod.nom_ape;
            usuario.email = user_mod.email;
            usuario.telefono = user_mod.telefono;
            usuario.direccion = user_mod.direccion;
            usuario.contraseña = user_mod.contraseña;
        } 
        console.log('Usuario modificado exitosamente');
        return res.send(usuario);
    });
}); 

/**
 * @swagger
 * /users/delete:
 *  delete:
 *    description: Elimina un usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */


server.delete('/users/delete', Admininistrador, (req,res) => {
    const {id} = req.body; 
    let user_id = req.body.id; 
    usuarios.forEach((usuario, i) => { 
        if (usuario.id == user_id){
            usuarios.splice(i, 1);
            console.log('Usuario eliminado exitosamente');
            return res.send('Usuario eliminado');
        }
    });
});  

/**
 * @swagger
 * /users:
 *  delete:
 *    description: Muestra los usuarios
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.get('/users', (req,res) => res.send(users));

// server listen 

server.listen(7000, (req,res) => { 
    console.log('Escuchando en el puerto 3000');
});
