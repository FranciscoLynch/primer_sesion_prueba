const express = require('express'); 
const server = express(); 

const autores = [ 
    { 
        id: 1, 
        nombre: 'Jorge Luis', 
        apellido: 'Borges', 
        fechaDeNacimiento: '24/08/1899', 
        libros: [ 
            { 
                id: 1, 
                titulo: 'Ficciones', 
                descripcion: 'Se trata de uno de sus más...', 
                anioPublicacion: 1944
            }, 
            { 
                id: 2, 
                titulo: 'El Aleph', 
                descripcion: 'Otra recopilacion de cuentos...', 
                anioPublicacion: 1949
            }
        ]
    }
];

// middlewares 

const controlDeAutores = (req, res, next) => { 
    let id_user = req.params.id;

    autores.forEach(autor => { 
        if (autor.id == id_user) { 
            next();
        }
    }); 

    res.send('Ese autor no existe.');
}

// endpoints  

server.get('/autores', function(req,res){ 
    res.send(autores);
}); 

// falta el post de /autores  

server.post('/autores', function(req,res){ 
    
});

server.get('/autores/:id', controlDeAutores,function(req,res){ 
    let id_user = req.params.id; 

    autores.forEach(autor => { 
        if (autor.id == id_user){ 
            res.send(autor);
        } 
    }); 
    res.send('Auto no encontrado');
}); 

// falta el delete y el put de autores/:id 

server.get('autores/:id/libros', controlDeAutores, function(req, res){ 
    let id_user = req.params.id; 

    autores.forEach(autor => { 
        if (autor.id == id_user){ 
            res.send(autor);
        } 
    }); 
    res.send('Auto no encontrado');
}); 

// falta el post de autores/:id/libros

server.get('autores/:id/libros/:idLibro', function(req,res) {
    let id_user = req.params.id; 
    let id_libro = req.params.id; 

    autores.forEach(autor => { 
        if (autor.id == id_user) { 
            autor.libros.forEach(libro => { 
                if (libro.id == id_libro){ 
                    res.send(libro);
                }
            })
        }
    }); 

    res.send('ID de libro invalido');
});

// falta el delete y el put de autores/:id/libros/:idLibro

server.listen(3001, function(){
    console.log('server corriendo');
}); 