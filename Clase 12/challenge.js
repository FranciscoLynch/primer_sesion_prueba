const express = require('express'); 
const app = express(); 

const logger = (req, res, next) => {
    console.log(`la ruta es ${req.path}`);
    next();
}

app.get('/cursos', (req, res) => { 
    res.send([
        {id: 1, nombre: 'Curso de cocina'}, 
        {id: 2, nombre: 'Curso de manejo'}, 
        {id: 3, nombre: 'Curso de programacion'}, 
        {id: 4, nombre: 'Curso de python'} 
    ]);
}); 

app.listen(4000, () => console.log('listening on 4000'));