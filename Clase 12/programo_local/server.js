const express = require('express'); 
const server = express(); 
const compression = require('compression'); 

server.use(express.json()); 
server.use(compression());

// middlewares 

    // Middleware de aplicacion > middleware que son usados a un nivel global del servidor, son las primeras ejecutadas antes de llegar a cualquier ruta. 

server.use(function(req, res, next){
    console.log(req.url); 
    // si no le coloca el express el endpoint nunca va a llegar a notificarse y no devolvera nada 
    next();
}); 

    // Middleware global, genero un log con cada ejecucion 
server.use(function(req,res,next){
    console.log('Time: ', Date.now());
    next();
}); 

    // Middleware de aplicacion 
function validarUsuario(req,res,next){ 
        if (req.query.usuario != 'admin'){ 
            res.json('usuario invalido');
        } else { 
            next();
        }
}

function interceptar(req, res, next){ 
    res.json('acceso denegado');
}

// endpoints 

server.get('/', function(req, res){ 
    res.json('hola mundo');
}); 

server.get('/saludo_query/usuario', validarUsuario,function(req,res){
    let data = req.query;
    res.json('hola mundo, usuario validado correctamente');
}); 

server.get('/demo', interceptar, function(req, res){
    res.json('Hola mundo, interceptado');
}); 

server.get('/probarcompression', function(req,res){
    const animal = 'alligator '; 
    res.send(animal.repeat(1000));
}); 




server.listen(3000, function () {
    console.log('servidor ejecutandose en el puerto 3000');
});