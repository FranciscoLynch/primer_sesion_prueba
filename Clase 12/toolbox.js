const express = require('express');
const app = express();

const logger = (req, res, next) => {
    console.log(`request HTTP method: ${req.method}`);
    next();
}

app.use(logger);

const esAdministrador = (req, res, next) => {
    const usuarioAdministrador = false;
    if (usuarioAdministrador) {
        console.log('El usuario esta correctamente logueado');
        next();
    } else {
        res.send('No esta logueado');
    }
};


app.get('/estudiantes', esAdministrador, (req, res) => {
    res.send([
        { id: 1, nombre: 'Lucas', edad: 35 }
    ])
});


app.post('/estudiantes', (req, res) => {
    res.status(201).send();
});

app.listen(5000, () => console.log('listening on 5000'));
