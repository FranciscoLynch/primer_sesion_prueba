const express = require('express');
const server = express();

server.use(express.urlencoded({ extended: true }));
server.use(express.json());

const users = [
    { id: 1, nombre: 'Pepe', email: 'pepe@nada.com' },
    { id: 2, nombre: 'Hugo', email: 'hugo@nada.com' },
    { id: 3, nombre: 'Juan', email: 'juan@nada.com' }
]

// middleware 

server.use(function (req, res, next) {
    console.log(req.url);
    next();
});

// endpoints

server.get('/users', function (req, res) {
    res.json(users);
});

server.get('users/:id', function (req, res) {
    let user_id = parseInt(req.params.id);
    let indice = false;
    let mi_respuesta = { user_id: user_id };

    // como buscar el nombre 

    /* opcion 1 */

    /* 
        users.forEach(function (value, index){
            console.log('value id: ' + value.id);
            console.log('user_id: ' + user_id);
            if (value.id == user_id){
                indice = index
            }
        }); 
    
        if (indice === false){
            mi_respuesta.msg = 'Usuario no encontrado'; 
        } else { 
            mi_respuesta.msg = 'Usuario encontrado';
            mi_respyesta.user = users[indice];
        }
    */

    /* opcion 2  */

    const result = users.filter(user => user.id == user_id);
    console.log(result[0]);
    if (result[0] === undefined) {
        mi_respuesta.msg = "Usuario no encontrado";
    } else {
        mi_respuesta.msg = "Usuario encontrado";
        mi_respuesta.user = result[0];
    }

    res.json(mi_respuesta);

});

server.post('/users_by_name', function(req,res){
    let name = req.body.name;
    let mi_respuesta = {search: name}; 

    const result = users.filter(user => user.nombre.toLocaleLowerCase() === name.toLocaleLowerCase()); 

    if (result.length == 0) {
        mi_respuesta.msg = "Usuario no encontrado";
    } else { 
        mi_respuesta.msg = "Usuario encontrado"; 
        mi_respuesta.users = result;
    } 

    res.json(mi_respuesta);

});

server.listen(3000, function () {
    console.log('Server corriendo');
});