const express = require('express');
const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Re-Workshop',
            version: '1.0.0'
        }
    },
    apis: ['./swagger.js'],
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));

// entidades 

const usuarios = [
    {
        id: 1,
        usuario: 'Pancho',
        nom_ape: 'Francisco Lynch',
        correo: 'pancho@gmail.com',
        direccion: 'asd 123',
        contra: 123,
        admin: true,
        login: false
    }
];

const productos = [
    {
        id: 1,
        name: 'Bagel de salmón',
        price: 425,
        short: 'BagSal'
    },
    {
        id: 2,
        name: 'Hamburguesa clásica',
        price: 350,
        short: 'HamClas'
    },
    {
        id: 3,
        name: 'Sandwich veggie',
        price: 310,
        short: 'SanVeggie'
    },
    {
        id: 4,
        name: 'Ensalada veggie',
        price: 340,
        short: 'VerdeVeggie'
    }
];

const pedidos = [
    {

    }
];

app.post('/registro', (req,res) => { 
    const { usuario_, nom_ape_, correo_, direccion_, contra_ } = req.body;
    const newUser = { 
        id: usuarios[usuarios.length - 1].id + 1,
        usuario: usuario_,
        nom_ape: nom_ape_,
        correo: correo_,
        direccion: direccion_,
        contra: contra_,
        admin: false,
        login: false
    }; 
    usuarios.push(newUser);
    res.send('La cuenta se ha creado exitosamente', newUser);
});

var indice;

app.put('/login', (req, res) => {
    const { usuario_, contra_ } = req.body;
    let flag = false;
    indice = usuarios.findIndex(user => user.usuario == usuario_);
    usuarios.map((usuario) => {
        if (usuario.usuario == usuario_ && usuario.contra == contra_) {
            usuario.login = true;
            flag = true;
        }
    });
    if (flag == true) {
        res.send('El usuario se ha logueado', indice);
    } else {
        res.send('El email o la contraseña son incorrectos', false);
    }
});

app.get('/verProductos', (req, res) => {
    if (indice != undefined) {
        res.send('Usuario valido', productos);
    } else {
        res.send('Usuario invalido', false)
    }
}); 


app.listen(4000, (req, res) => {
    console.log('Servidor corriendo');
});