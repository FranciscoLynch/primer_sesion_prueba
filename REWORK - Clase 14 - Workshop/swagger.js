/**
 * @swagger
 * /registro:
 *  post:
 *    description: Registrar un usuario
 *    parameters:
 *    - name: usuario
 *      type: string
 *      in: formData
 *      required: false
 *      description : Nombre de usuario
 *    - name: nom_ape
 *      type: string
 *      in: formData
 *      required: false
 *      description : Nombre y apellido del usuario
 *    - name: correo
 *      type: string
 *      in: formData
 *      required: false
 *      description : Correo electronico del usuario
 *    - name: direccion
 *      type: string
 *      in: formData
 *      required: false
 *      description : Direccion de envio del usuario
 *    - name: contra
 *      type: string
 *      in: formData
 *      required: false
 *      description : Contraseña del usuario
 *    responses:
 *      200:
 *        Sucess
 */


/**
 * @swagger
 * /login:
 *  put:
 *    description: Iniciar sesión
 *    parameters:
 *    - name: usuario
 *      type: string
 *      in: formData
 *      required: false
 *      description : Nombre del usuario
 *    - name: contra
 *      type: string
 *      in: formData
 *      required: false
 *      description : Contraseña del usuario
 *    responses:
 *      200:
 *        Sucess
 */

/**
 * @swagger
 * /verProductos:
 *  get:
 *    description: Mostrar todos los productos
 *    responses:
 *      200:
 *        Sucess
 */
