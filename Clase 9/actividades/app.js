const funciones = require('../actividades/calculator'); 
const crear_archivo = require('../actividades/crear');
const fs = require('fs'); 

const suma = funciones.sumar(2, 4);  
const resta = funciones.restar(10, 2); 
const multiplicar = funciones.multiplicar(7, 4);
const dividir = funciones.dividir(15, 3);

const array = [suma, resta, multiplicar, dividir]; 

array.forEach(arr => { 
    fs.appendFile('log.txt',arr + "\n", err => { 
        if (err) { 
            console.log(err);
        }
    });
});  

array.forEach(item => { 
    crear_archivo.crear('archivo.txt', item);
}); 

