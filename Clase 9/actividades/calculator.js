function sumar(n1, n2){  
    if (isNaN(n1)===false || isNaN(n2)===false){
        return n1 + n2;
    } else { 
        console.log("error");
    } 
} 

function restar(n1, n2){ 
    if (isNaN(n1)===false || isNaN(n2)===false){
        return n1 - n2;
    } else { 
        console.log("error");
    } 
} 

function multiplicar(n1, n2){ 
    if (isNaN(n1)===false || isNaN(n2)===false){
        return n1 * n2;
    } else { 
        console.log("error");
    } 
}  

function dividir(n1, n2){ 
    if (isNaN(n1)===false || isNaN(n2)===false){
        return n1 / n2;
    } else { 
        console.log("error");
    } 
} 

exports.sumar = sumar; 
exports.restar = restar;
exports.multiplicar = multiplicar; 
exports.dividir = dividir;  
