const express = require('express');
const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

// configuracion de middleware para aceptar json en su formato 

app.use(express.json()) // for pasing 

// configurar el option 

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Acamica API',
            version: '1.0.0'
        }
    },
    apis: ['./challenge.js']
};

// configurar el swaggerdocs 

const swaggerDocs = swaggerJsDoc(swaggerOptions);

// configuro el server para el uso del swagger, o configurar el endpoint 

app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));


/**
 * @swagger
 * /estudiantes:
 *  post:
 *    description: Crea un nuevo estudiante
 *    parameters:
 *    - name: nombre
 *      description: Nombre del estudiante
 *      in: formData
 *      required: true
 *      type: string
  *    - name: edad
 *      description: Edad del estudiante
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.post('/estudiantes', (req, res) => {
    res.status(201).send();
});


/**
 * @swagger
 * /estudiantes:
 *  get:
 *    description: Actualiza los estudiantes
 *    responses:
 *      200:
 *        description: Success *
 */

app.get('/estudiantes', (req, res) => {
    res.status(201).send('Hola estudiantes');
});


/**
 * @swagger
 * /estudiantes:
 *  put:
 *    description: Actualiza los estudiantes
 *    responses:
 *      200:
 *        description: Success *
 */

app.put('/estudiantes', (req, res) => {
    res.status(201).send();
});

/**
 * @swagger
 * /estudiantes:
 *  patch:
 *    description: Actualiza los estudiantes
 *    responses:
 *      200:
 *        description: Success *
 */

app.patch('/estudiantes', (req, res) => {
    res.status(201).send();
});


/**
 * @swagger
 * /estudiantes:
 *  delete:
 *    description: Elimina un estudiante
 *    responses:
 *      200:
 *        description: Success *
 */

app.delete('/estudiantes', (req, res) => {
    res.status(201).send();
});

app.listen(3000, function () {
    console.log('estoy escuchando por el puerto 3000')
});