const express = require('express');
const foro = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

foro.use(express.json());
foro.use(express.urlencoded({ extended: true }));

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'MySwagger',
            version: '1.0.0'
        }
    },
    apis: ['./index.js']
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

foro.use('/api-docs',
    swaggerUi.serve,
    swaggerUi.setup(swaggerDocs));


const usuarios = [
    {
        id: 1,
        nombre: 'Juan',
        apellido: 'Perez',
        email: 'juan@nada.com'
    }
];

const topicos = [
    {
        id: 1,
        titulo: 'programacion',
        descripcion: 'La programacion es...'
    }
];

const comentarios = [
    {
        id: 1,
        topicoID: topicos.id,
        usuarioID: usuarios.id,
        comentario: ' '
    }
];

// manejo de usuarios 

/**
 * @swagger
 * /users:
 *  get:
 *    description: Devuelve la lista de los usuarios
 *    responses:
 *      200:
 *        description: Success
 * 
 */


foro.get('/users', (req, res) => res.send(usuarios));


/**
 * @swagger
 * /user_high:
 *  post:
 *    description: Crea un nuevo usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: nombre
 *      description: Nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: Apellido del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */ 

foro.post('/user_high', (req, res) => {
    const { id, nombre, apellido, email } = req.body;
    const usuario = {
        id: id,
        nombre: nombre,
        apellido: apellido,
        email: email
    };
    usuarios.push(usuario);
    console.log('Usuario creado exitosamente');
    return res.send(usuario);
});
/* 
foro.post('/user_low', function(req,res){ 

});
 */ 


/**
 * @swagger
 * /modify_user:
 *  put:
 *    description: Modifica usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: nombre
 *      description: Nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: Apellido del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */


foro.put('/modify_user', (req, res) => {
    const { id, nombre, apellido, email } = req.body;
    const mod_user = {
        id: id,
        nombre: nombre,
        apellido: apellido,
        email: email
    };
    let id_user = mod_user.id;
    usuarios.forEach(usuario => {
        if (usuario.id == id_user) {
            usuario.id = mod_user.id;
            usuario.nombre = mod_user.nombre;
            usuario.apellido = mod_user.apellido;
            usuario.email = mod_user.email;
        }
        console.log('Usuario modificado exitosamente');
        return res.send(usuario);
    });
});


/**
 * @swagger
 * /delete_user:
 *  delete:
 *    description: Elimina un usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */ 


foro.delete('/delete_user', (req, res) => {
    const { id } = req.body;
    let id_user = req.body.id;
    usuarios.forEach((usuario, i) => {
        if (usuario.id = id_user) {
            usuarios.splice(i, 1);
            console.log('Usuario eliminado exitosamente');
            return res.send('Usuario eliminado');
        }
    });
});

// manejo de topicos 



/**
 * @swagger
 * /topics:
 *  get:
 *    description: Devuelve la lista de los topicos
 *    responses:
 *      200:
 *        description: Success
 * 
 */


foro.get('/topics', (req, res) => res.send(topicos));



/**
 * @swagger
 * /topic_high:
 *  post:
 *    description: Crea un nuevo topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: titulo
 *      description: Titulo del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: descripcion
 *      description: Descripción del topico
 *      in: formData
 *      required: true
 *      type: string
 * 
 */ 


foro.post('/topic_high',(req, res) => {
    const { id, titulo, descripcion } = req.body;
    const topic = {
        id: id,
        titulo: titulo,
        descripcion: descripcion
    };
    topicos.push(topic);
    console.log('Topico creado exitosamente');
    return res.send(topic);
});
/* 
foro.put('/topic_low', function(req,res){ 

});
 */ 


/**
 * @swagger
 * /modify_topic:
 *  put:
 *    description: Modifica topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: titulo
 *      description: Titulo del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: descripcion
 *      description: Descripción del topico
 *      in: formData
 *      required: true
 *      type: string
 * 
 */
 

foro.put('/modify_topic', (req, res) => {
    const { id, titulo, descripcion } = req.body;
    const mod_topic = {
        id: id,
        titulo: titulo,
        descripcion: descripcion
    };
    let id_topic = mod_topic.id;
    topicos.forEach(topic => {
        if (topic.id == id_topic) {
            topic.id = mod_topic.id;
            topic.titulo = mod_topic.titulo;
            topic.descripcion = mod_topic.descripcion;
        }
        console.log('Topico modificado exitosamente');
        return res.send(topic);
    })
});


/**
 * @swagger
 * /delete_topic:
 *  delete:
 *    description: Elimina un topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */ 



foro.delete('/delete_topic',(req, res) => {
    const { id } = req.body; 
    let id_topic = req.body.id; 
    topicos.forEach((topic, i) => { 
        if (topic.id == id_topic){ 
            topicos.splice(i, 1); 
            console.log('Topico eliminado exitosamente'); 
            return res.send('Topico eliminado');
        }
    });
}); 

// manejo de comentarios   


/**
 * @swagger
 * /comments:
 *  get:
 *    description: Devuelve la lista de los comentarios
 *    responses:
 *      200:
 *        description: Success
 * 
 */


foro.get('/comments', (req, res) => res.send(comentarios)); 


/**
 * @swagger
 * /comment_high:
 *  post:
 *    description: Crea un nuevo topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: topico_id
 *      description: ID del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: user_id
 *      description: ID del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: comentario
 *      description: Comentario del tópico
 *      in: formData
 *      required: true
 *      type: string
 * 
 */


foro.post('/comment_high',(req, res) => {
    const { id, topicoID, usuarioID, comentario } = req.body;
    const comment = {
        id: id,
        topicoID: topicoID,
        usuarioID: usuarioID, 
        comentario: comentario
    };
    comentarios.push(comment);
    console.log('Comentario creado exitosamente');
    return res.send(comment);
});
/* 
foro.put('/comment_low', function(req,res){ 

});
 */ 



/**
 * @swagger
 * /modify_comment:
 *  put:
 *    description: Modifica topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: topico_id
 *      description: ID del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: user_id
 *      description: ID del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: comentario
 *      description: Comentario del tópico
 *      in: formData
 *      required: true
 *      type: string
 * 
 */


foro.put('/modify_comment',(req, res) => {
    const { id, topicoID, usuarioID, comentario } = req.body;
    const mod_comment = {
        id: id,
        topicoID: topicoID,
        usuarioID: usuarioID, 
        comentario: comentario
    };
    let id_comment = mod_comment.id;
    topicos.forEach(comment => {
        if (comment.id == id_comment) {
            comment.topicoID = mod_comment.id;
            comment.usuarioID = mod_comment.titulo;
            comment.comentario = mod_comment.descripcion;
        }
        console.log('Comentario modificado exitosamente');
        return res.send(comment);
    })
});


/**
 * @swagger
 * /delete_comment:
 *  delete:
 *    description: Elimina un comentario
 *    parameters:
 *    - name: id
 *      description: Id del comentario
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */


foro.delete('/delete_comment',(req, res) => {
    const { id } = req.body; 
    let id_comment = req.body.id; 
    comentarios.forEach((comment, i) => { 
        if (comment.id == id_comment){ 
            comentarios.splice(i, 1); 
            console.log('Comentario eliminado exitosamente'); 
            return res.send('Comentario eliminado');
        }
    });
}); 

foro.listen(4000, function () {
    console.log('estoy escuchando por el puerto 4000')
});