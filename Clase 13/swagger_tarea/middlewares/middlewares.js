// middlewares de usuario 

// lista de usuarios 

/**
 * @swagger
 * /users:
 *  get:
 *    description: Devuelve la lista de los usuarios
 *    responses:
 *      200:
 *        description: Success
 * 
 */

// dar de alta

/**
 * @swagger
 * /user_high:
 *  post:
 *    description: Crea un nuevo usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: nombre
 *      description: Nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: Apellido del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */ 

// modificar

/**
 * @swagger
 * /modify_user:
 *  put:
 *    description: Modifica usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: nombre
 *      description: Nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: Apellido del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

// eliminar

/**
 * @swagger
 * /delete_user:
 *  delete:
 *    description: Elimina un usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */ 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

// middlewares de topicos 

// lista de topicos 


/**
 * @swagger
 * /topics:
 *  get:
 *    description: Devuelve la lista de los topicos
 *    responses:
 *      200:
 *        description: Success
 * 
 */

// dar de alta 


/**
 * @swagger
 * /topic_high:
 *  post:
 *    description: Crea un nuevo topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: titulo
 *      description: Titulo del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: descripcion
 *      description: Descripción del topico
 *      in: formData
 *      required: true
 *      type: string
 * 
 */ 

// modificar 


/**
 * @swagger
 * /modify_topic:
 *  put:
 *    description: Modifica topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: titulo
 *      description: Titulo del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: descripcion
 *      description: Descripción del topico
 *      in: formData
 *      required: true
 *      type: string
 * 
 */

// eliminar 


/**
 * @swagger
 * /delete_topic:
 *  delete:
 *    description: Elimina un topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */ 



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// middlewares de comentarios 

// lista de comentarios

/**
 * @swagger
 * /comments:
 *  get:
 *    description: Devuelve la lista de los comentarios
 *    responses:
 *      200:
 *        description: Success
 * 
 */

// dar de alta 

/**
 * @swagger
 * /comment_high:
 *  post:
 *    description: Crea un nuevo topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: topico_id
 *      description: ID del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: user_id
 *      description: ID del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: comentario
 *      description: Comentario del tópico
 *      in: formData
 *      required: true
 *      type: string
 * 
 */

// modificar


/**
 * @swagger
 * /modify_comment:
 *  put:
 *    description: Modifica topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: topico_id
 *      description: ID del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: user_id
 *      description: ID del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: comentario
 *      description: Comentario del tópico
 *      in: formData
 *      required: true
 *      type: string
 * 
 */

// eliminar 


/**
 * @swagger
 * /delete_comment:
 *  delete:
 *    description: Elimina un comentario
 *    parameters:
 *    - name: id
 *      description: Id del comentario
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */
