const express = require('express');
const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

// configuracion de middleware para aceptar json en su formato 

app.use(express.json()); // for pasing 
app.use(express.urlencoded({ extended:true})); // for pasing application/x- www-form-urlencoded

/*
 * 
*Marca (char)
Modelo (char)
Fecha de fabricación (date)
Cantidad de puertas (entero)
Disponible para la venta (booleano)
*/

//1.instalamos swaggerUI

//npm i swagger-ui-express swagger-jsdoc --save

//2.importamos las librerias al proyecto

//3.definicion de opciones

// configurar el option 

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Acamica API',
            version: '1.0.0'
        }
    },
    apis: ['./challenge.js']
};

//4. configurar el swaggerdocs 

const swaggerDocs = swaggerJsDoc(swaggerOptions); 

//5. configuracion de endpoint de swagger 

app.use('/api-docs', swaggerUI.serve,swaggerUI.setup(swaggerDocs));

//6. configuracion de endpoint con swagger 

let vehiculos = [
    {
        marca:"Ford",
        modelo:"Ford EcoSport.",
        fecha_fabricacion: new Date(25/10/2000),
        puertas: 4,
        disponible:true
    },
    {
        marca:"Mazda",
        modelo:"Mazda 3.",
        fecha_fabricacion: new Date(2/05/2015),
        puertas: 5,
        disponible:false
    }
];
 
/**
 * @swagger
 * /vechiculos:
 *  get:
 *    description: Lista nuevos vehiculos 
 *    responses:
 *      200:
 *        description: Success
 * 
 */

 app.get('/vehiculos', function(req, res) {
    return res.send(vehiculos);
}); 

/**
 * @swagger 
 * /vehiculos: 
 *  post: 
 *    description: Agregar vehiculos 
 *    parameters: 
 *    - name: marca 
 *      description: Marca carro 
 *      in: formData 
 *      required: true 
 *      type: string
 *    - name: modelo 
 *      description: Modelo del carro 
 *      in: formData 
 *      required: true 
 *      type: string 
 *    - name: fecha 
 *      description: fecha de fabricacion 
 *      in: formData 
 *      required: true 
 *      type: string 
 *    - name: puertas 
 *      description: numero de puertas
 *      in: formData 
 *      required: true 
 *      type: string 
 *    - name: disponible 
 *      description: Disponibilidad 
 *      in: formData 
 *      required: true 
 *      type: boolean
 *    responses: 
 *      200: 
 *        description: Success
 * 
 */

 app.post('/vehiculos', function(req, res) {
    const { marca,modelo,fecha,puertas,disponible} = req.body; 
    const vehiculo = { 
        marca:marca, 
        modelo:modelo,
        fecha_fabricacion:fecha,
        puertas:puertas, 
        disponile:disponible
    }
    vehiculos.push(vehiculo);
    return res.send(vehiculos);
}); 

/**
 * @swagger
 * /vechiculos:
 *  delete:
 *    description: borrar por nombre marca
 *    parameters:
 *    - name: marca
 *      in: formData
 *      description: nombre marca      
 *      required: true
 *      type: string 
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.delete('/vehiculos', function(req, res){ 
    console.log(req.body); 
    if (!req.body.marca) {
        respuesta = { 
            error: true, 
            codigo: 502, 
            mensaje: 'El campo nombre es requerido',
        };
    } else { 
        if (req.body.marca !== "") { 
            let marca_data = req.body.marca; 
            let filter_data = vehiculos.filter((x) => x.marca !== marca_data); 
            vehiculos = [...filter_data];
        }
    } 
    res.send(vehiculos);
})



/**
 * @swagger
 * /vechiculos:
 *  put:
 *    description: actualizar un vehiculo por marca
 *    parameters:
 *    - name: marca
 *      description: Marca carro
 *      in: formData
 *      required: true
 *      type: string
 *    - name: modelo
 *      description: model del carro
 *      in: formData
 *      required: true
 *      type: string
 *    - name: fecha
 *      description: fecha fabricacion
 *      in: formData
 *      required: true
 *      type: string
 *    - name: puertas
 *      description: numero de puertas
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: disponible
 *      description: disponibilidad
 *      in: formData
 *      required: true
 *      type: boolean
 *    responses:
 *      200:
 *        description: Success
 * 
 */ 

app.put('/vehiculos', function(req,res){ 
    console.log(req.body); 
    if (!req.body.marca) {
        respuesta = { 
            error: true, 
            codigo: 502, 
            mensaje: 'El campo nombre es requerido',
        }; 
    } else { 
        if (req.body.marca !== "") {
            const { marca,modelo,fecha,puertas,disponible} = req.body; 
            vehiculos.map((item) => { 
                if (item.marca === marca) {
                    item.modelo = modelo;
                    item.fecha_fabricacion = fecha; 
                    item.puertas = puertas; 
                    item.disponible = disponible;
                }
            }); 

            respuesta = { 
                error: false, 
                codigo: 200, 
                mensaje: "Marca " + marca + " de vehiculos actualizado"
            }; 
            console.log(respuesta);
        }
    } 
    res.send(vehiculos);
});

app.listen(3001, function(){
    console.log('Escuchando en el puerto 3001')
});
