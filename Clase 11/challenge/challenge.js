const express = require('express'); 
const app = express(); 
const array = ["Juan Manuel", 18, "Martin", 29, "Ricardo", 53]; 

app.get('/', function (req, res) {
    res.send(array);
  });
  
  app.listen(3000, function () {
    console.log('Escuchando el puerto 3000!');
  });
